using Microsoft.AspNetCore.Mvc;

namespace WebApplication1.Controllers
{
    [ApiController]
    [Route("api")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet("numberlist/{k:int}")]
        public IActionResult NumberList(int k)
        {
            if (k < 1)
            {
                return BadRequest("Number cant's be smaller than 1.");
            }
            else if (k > 100)
            {
                return BadRequest("Number can't be greater than 100.");
            }
            else
            {
                int[] numbers = new int[k];
                for (int i = 0; i < k; i++)
                {
                    numbers[i] = i + 1;
                }
                return Ok(numbers);
            }
        }
        /*
        public int[] NumberLister(int k)
        {
            int[] numbers = new int[k];
            for (int i = 0; i < k; i++)
            {
                numbers[i] = i + 1;
            }
            return numbers;
        }
        */
        [HttpGet("weatherforecast")]
        public List<string> Get()
        {
            List<string> myList = new List<string>()
            {
                "Niko", "Henna", "Lucifer"
            };
            return myList;
            /*
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateOnly.FromDateTime(DateTime.Now.AddDays(index)),
                TemperatureC = Random.Shared.Next(-20, 55),
                Summary = Summaries[Random.Shared.Next(Summaries.Length)]
            })
            .ToArray();
            */
        }
    }
}