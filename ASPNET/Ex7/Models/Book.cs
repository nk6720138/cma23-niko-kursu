﻿using System;
using System.Collections.Generic;

namespace Ex7.Models;

public partial class Book
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public int ReleaseYear { get; set; }

    public int GenreId { get; set; }

    public int LanguageId { get; set; }

    public virtual Genre Genre { get; set; } = null!;

    public virtual Language Language { get; set; } = null!;

    public virtual ICollection<Volume> Volumes { get; set; } = new List<Volume>();
}
