﻿using System;
using System.Collections.Generic;

namespace Ex7.Models;

public partial class Volume
{
    public int Id { get; set; }

    public int BookId { get; set; }

    public virtual Book Book { get; set; } = null!;

    public virtual ICollection<Borrow> Borrows { get; set; } = new List<Borrow>();
}
