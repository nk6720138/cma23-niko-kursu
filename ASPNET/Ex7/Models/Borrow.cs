﻿using System;
using System.Collections.Generic;

namespace Ex7.Models;

public partial class Borrow
{
    public int Id { get; set; }

    public int VolumeId { get; set; }

    public int UserId { get; set; }

    public DateTime BorrowedAt { get; set; }

    public DateTime DueDate { get; set; }

    public DateTime? ReturnedAt { get; set; }

    public virtual User User { get; set; } = null!;

    public virtual Volume Volume { get; set; } = null!;
}
