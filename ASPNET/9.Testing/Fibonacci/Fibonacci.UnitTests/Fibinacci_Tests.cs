using NUnit.Framework;
using FibonacciLib = Fibonacci.Library.Fibonacci;
namespace Fibonacci.UnitTests
{
    [TestFixture]
    public class Fibonacci_Tests
    {
        [TestCase(0, 0)]
        [TestCase(1, 1)]
        [TestCase(2, 1)]
        [TestCase(3, 2)]
        [TestCase(4, 3)]
        [TestCase(5, 5)]
        public void Recursive_WhenNIsInBounds_ShouldReturnNthFibonacci(int n, long expected) =>
            Assert.AreEqual(expected, FibonacciLib.Recursive(n));
        
        [TestCase(int.MaxValue)]
        [TestCase(-1)]
        public void Recursive_WhenNIsOutOfBounds_ShouldThrowArgumentOutOfRange(int n) =>
            Assert.Throws<ArgumentOutOfRangeException>(() => FibonacciLib.Recursive(n));

    }
}