using NUnit.Framework;
using DoXLib = DoX.Library.DoXClass;
namespace DoX.UnitTests
{
    [TestFixture]
    public class DoX_Tests
    {
        [Test]
        public void DoX_WhenN0_ShouldReturn1()
        {
            Assert.AreEqual(expected: 1, actual: DoXLib.TryGetFactorial(nStartingValue: 0));
        }
        [TestCase(0, 1)]
        [TestCase(1, 1)]
        [TestCase(2, 2)]
        [TestCase(15, 1307674368000)]
        public void DoX_WhenNInRange_ShouldReturnNFactorial(int n, long expected)
        {
            Assert.AreEqual(expected, DoXLib.TryGetFactorial(n));
        }

        [TestCase(int.MaxValue)]
        [TestCase(-1)]
        public void DoX_WhenNOutOfRange_ShouldThrowArgumentOutOfRange(int n)
            => Assert.Throws<ArgumentOutOfRangeException>(() => DoXLib.TryGetFactorial(n));
        
    }
}