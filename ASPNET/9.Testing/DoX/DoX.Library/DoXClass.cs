﻿namespace DoX.Library
{
    public static class DoXClass
    {
        public static long TryGetFactorial(int nStartingValue)
        {
            if (nStartingValue < 0 || nStartingValue > 150)
                throw new ArgumentOutOfRangeException("n is out of bounds!");

            long getFactorial(int n) => n > 0 ? n * getFactorial(n - 1) : 1;

            return getFactorial(nStartingValue);
        }
    }
}