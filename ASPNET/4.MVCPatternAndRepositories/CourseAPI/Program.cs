using CourseAPI.Repositories;
using Microsoft.EntityFrameworkCore;

namespace CourseAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.

            builder.Services.AddControllers().AddNewtonsoftJson();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();


            //Commented this out to fix the program, but why exactly does/doesn't it work?
            //builder.Services.AddSingleton<ICourseRepository, CourseRepository>();

            builder.Services.AddDbContext<CoursesContext>(options => options.UseNpgsql(
                @"Server=PostgreSQL 16; Host=localhost;Port=5432;Username=postgres;Password=tu86hao;Database=course_db"));
            builder.Services.AddScoped<ICourseRepository, CourseRepository>();
            builder.Services.AddControllers().AddNewtonsoftJson();

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }            

            app.UseAuthorization();

            app.MapControllers();

            app.Run();
        }
    }
}