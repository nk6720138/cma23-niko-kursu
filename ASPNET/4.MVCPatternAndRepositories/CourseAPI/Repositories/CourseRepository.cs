﻿using CourseAPI.Models;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace CourseAPI.Repositories
{
    public class CourseRepository : ICourseRepository
    {
        private readonly CoursesContext _context;

        public CourseRepository(CoursesContext context)
        {
            _context = context;
        }

        //get course from database by its ID
        public Course GetCourse(int id)
        {
            if (_context.Courses.ToList().Exists(c => c.Id == id))
            {
                return _context.Courses.FirstOrDefault(c => c.Id == id);
            }
            return null;
        }

        //get all courses from the database
        public List<Course> GetAllCourses() => _context.Courses.ToList();

        //Add a course to the database
        public void AddCourse(Course course)
        {
            _context.Courses.Add(course);
            _context.SaveChanges();
        }

        //Update an existing course in the database
        public void UpdateCourse(int id, Course newCourse)
        {
            if (GetCourse(id) != null)
            {
                Course course = GetCourse(id);
                course.Name = newCourse.Name;
                course.Credits = newCourse.Credits;
                _context.Courses.Update(course);
                _context.SaveChanges();
            }
        }
        //Remove a course from the database
        public void DeleteCourse(int id)
        {
            if (GetCourse(id) != null)
            {
                Course course = GetCourse(id);
                _context.Courses.Remove(course);
                _context.SaveChanges();
            }
        }
        //Patch
        public void PatchCourse(int id, [FromBody] JsonPatchDocument<Course> patchDocument)
        {
            var course = GetCourse(id);
            if (course != null)
            {
                patchDocument.ApplyTo(course);
                _context.SaveChanges();
            }
        }
    }
}
