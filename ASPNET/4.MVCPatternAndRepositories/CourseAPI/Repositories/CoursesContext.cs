﻿using CourseAPI.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace CourseAPI.Repositories
{
    public class CoursesContext : DbContext
    {
        public DbSet<Course> Courses { get; set; }
        public CoursesContext(DbContextOptions<CoursesContext> options) : base(options) { }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Course>().HasData(
                new Course { Id = 11, Name = "Math", Credits = 10 },
                new Course { Id = 22, Name = "Silly Walking", Credits = 12 },
                new Course { Id = 333, Name = "Advanced Plumbing", Credits = 3 }, 
                new Course { Id = 444, Name = "Gym", Credits = 5}
            );
        }
    }
}
