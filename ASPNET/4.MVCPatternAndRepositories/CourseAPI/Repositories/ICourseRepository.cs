﻿using CourseAPI.Models;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace CourseAPI.Repositories
{
    public interface ICourseRepository
    {
        public Course GetCourse(int id);
        public List<Course> GetAllCourses();
        public void AddCourse(Course course);
        public void UpdateCourse(int id, Course newCourse);
        public void DeleteCourse(int id);
        public void PatchCourse(int id, [FromBody] JsonPatchDocument<Course> patchDocument);
    }
}
