﻿using CourseAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CourseAPI.Repositories;
using Microsoft.AspNetCore.JsonPatch;

namespace CourseAPI.Controllers
{
    [Route("api/courses")]
    [ApiController]
    public class CoursesController : ControllerBase
    {
        private readonly ICourseRepository _courseRepository;
        public CoursesController(ICourseRepository courseRepository)
        {
            _courseRepository = courseRepository;
        }
        //get all courses
        [HttpGet]
        public IActionResult GetCourses()
        {
            return Ok(_courseRepository.GetAllCourses());
        }
        //get a course with given ID
        [HttpGet("{id:int}")]
        public IActionResult GetCourse(int id)
        {
            Course course = _courseRepository.GetCourse(id);
            if (course != null)
            {
                return Ok(course); 
            }
            else return NotFound("There is no course with this ID");
        }
        //Delete a course with given ID
        [HttpDelete("{id:int}")]
        public IActionResult DeleteCourse(int id)
        {
            Course course = _courseRepository.GetCourse(id);
            if (course != null)
            {
                _courseRepository.DeleteCourse(id);
                return Ok();
            }
            else return NotFound("There is no course with this ID");
        }
        //create a course. ID has to be given.
        [HttpPost]
        public IActionResult CreateACourse([FromBody] Course course)
        {
            if (_courseRepository.GetCourse(course.Id) != null)
            {
                return BadRequest("A course with this ID already exists!");
            }
            
            Course newCourse = new Course(course.Id, course.Name, course.Credits);
            if (!TryValidateModel(newCourse))
            {
                return BadRequest(ModelState);
            }

            _courseRepository.AddCourse(newCourse);
            return Ok();
        }
        //Updates a course. The ID cannot be changed from the body. The ID must be unique. and a positive integer.
        [HttpPut("{id:int}")]
        public IActionResult UpdateCourse(int id, [FromBody] Course newCourse)
        {
            if (_courseRepository.GetCourse(id) == null)
            {
                return BadRequest("There is no course with this ID");
            }

            newCourse.Id = id;

            _courseRepository.UpdateCourse(id, newCourse);
            return Ok();
        }
        //Patch a course
        [HttpPatch("{id:int})")]
        public IActionResult PatchCourse(int id, [FromBody] JsonPatchDocument<Course> patchDocument)
        {
            if (_courseRepository.GetCourse(id) == null)
            {
                return BadRequest("There is no course with this ID");
            }
            _courseRepository.PatchCourse(id, patchDocument);
            return Ok();
        }
    }
}

