﻿namespace CourseAPI.Models
{
    public class Lecture
    {
        public int Id { get; set; }
        public int CourseId { get; set; }
        public DateTime StartTime { get; set; }
        public int Length { get; set; }
        public Course ? Course { get; set; }
    }
}
