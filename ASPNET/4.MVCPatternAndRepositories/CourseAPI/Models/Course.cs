﻿using System.ComponentModel.DataAnnotations;

namespace CourseAPI.Models
{
    public class Course
    {
        [Required]
        [Range(1, Int32.MaxValue)] //positive ints, a placeholder
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        [Range(1 , 20, ErrorMessage = "Must have between 1 and 20 credits")]
        public int Credits { get; set; }
        public ICollection<Lecture> Lectures { get; set; }

        public Course() { }
        public Course(int id, string name, int credits)
        {
            Id = id;
            Name = name;
            Credits = credits;
        }        
    }
}
