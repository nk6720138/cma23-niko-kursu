﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CourseAPI.Migrations
{
    /// <inheritdoc />
    public partial class AddLectures2 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Lecture_Courses_courseId",
                table: "Lecture");

            migrationBuilder.RenameColumn(
                name: "courseId",
                table: "Lecture",
                newName: "CourseId");

            migrationBuilder.RenameIndex(
                name: "IX_Lecture_courseId",
                table: "Lecture",
                newName: "IX_Lecture_CourseId");

            migrationBuilder.AddForeignKey(
                name: "FK_Lecture_Courses_CourseId",
                table: "Lecture",
                column: "CourseId",
                principalTable: "Courses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Lecture_Courses_CourseId",
                table: "Lecture");

            migrationBuilder.RenameColumn(
                name: "CourseId",
                table: "Lecture",
                newName: "courseId");

            migrationBuilder.RenameIndex(
                name: "IX_Lecture_CourseId",
                table: "Lecture",
                newName: "IX_Lecture_courseId");

            migrationBuilder.AddForeignKey(
                name: "FK_Lecture_Courses_courseId",
                table: "Lecture",
                column: "courseId",
                principalTable: "Courses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
