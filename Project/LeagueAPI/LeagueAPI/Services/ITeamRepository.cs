﻿using LeagueAPI.Models;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
namespace LeagueAPI.Services
{
    public interface ITeamRepository
    {
        public List<Team> GetLeaderBoard();
        public List<Team> GetTeams();
        public Team GetTeamById(int id);
        public void AddTeam(Team team);
        public void RemoveTeam(int id);
        public void UpdateTeamWithId(int id, Team team);
        public void PatchTeamWithId(int id, [FromBody] JsonPatchDocument<Team> patchDocument);
        public bool TeamWithIdExists(int id);
    }
}
