﻿using LeagueAPI.Models;
using LeagueAPI.Repositories;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace LeagueAPI.Services
{
    public class MatchRepository : IMatchRepository
    {
        private readonly LeagueContext _context;
        public MatchRepository(LeagueContext context)
        {
            _context = context;
        }
        public void AddMatch(Match match)
        {
            _context.Matches.Add(match);
            _context.SaveChanges();
        }

        public Match GetMatchById(int id)
        {
            return _context.Matches.First(m => m.Id == id);
        }

        public List<Match> GetMatches()
        {
            return _context.Matches.ToList();
        }

        public bool MatchWithIdExists(int id)
        {
            return _context.Matches.ToList().Exists(m => m.Id == id);
        }

        public void PatchMatchWithId(int id, [FromBody] JsonPatchDocument<Match> patchDocument)
        {
            Match match = GetMatchById(id);
            patchDocument.ApplyTo(match);
            _context.SaveChanges();
        }

        public void RemoveMatchById(int id)
        {
            _context.Matches.Remove(GetMatchById(id));
            _context.SaveChanges();
        }

        public void UpdateMatchWithId(int id, [FromBody] Match newMatch)
        {
            Match match = GetMatchById(id);
            match.Date = newMatch.Date;
            match.HomeTeamId = newMatch.HomeTeamId;
            match.VisitorTeamId = newMatch.VisitorTeamId;
            match.VenueId = newMatch.VenueId;
            match.HomeTeamScore = newMatch.HomeTeamScore;
            match.VisitorTeamScore = newMatch.VisitorTeamScore;
            _context.Matches.Update(match);
            _context.SaveChanges();
        }
    }
}
