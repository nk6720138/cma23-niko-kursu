﻿using LeagueAPI.Models;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace LeagueAPI.Services
{
    public interface IVenueRepository
    {
        public List<Venue> GetVenues();
        public Venue GetVenueById(int id);
        public void AddVenue(Venue venue);
        public void RemoveVenue(int id);
        public void UpdateVenueWithId(int id, [FromBody] Venue venue);
        public void PatchVenueWithId(int id, [FromBody] JsonPatchDocument<Venue> patchDocument);
        public bool VenueWithIdExists(int id);
    }
}
