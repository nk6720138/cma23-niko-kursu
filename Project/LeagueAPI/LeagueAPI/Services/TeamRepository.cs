﻿using LeagueAPI.Models;
using LeagueAPI.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace LeagueAPI.Services
{
    public class TeamRepository : ITeamRepository
    {
        private readonly LeagueContext _context;

        public TeamRepository(LeagueContext context)
        {
            _context = context;
        }

        //Get a list of the teams in order from first to last by the amount of won games
        public List<Team> GetLeaderBoard()
        {
            var query = from team in _context.Teams
                        join match in _context.Matches
                        on team.Id equals match.HomeTeamId
                        where match.HomeTeamScore > 4
                        || (team.Id == match.VisitorTeamId && match.VisitorTeamScore > 4)
                        group team by team.Id into teamGroup
                        orderby teamGroup.Count() descending
                        select teamGroup.First();

            List<Team> teamsInOrderOfMatchesWon = query.ToList();

            return teamsInOrderOfMatchesWon;
        }

        //Get all teams
        public List<Team> GetTeams()
        {
            return _context.Teams.ToList();
        }

        //Get a team
        public Team GetTeamById(int id)
        {
            return _context.Teams.First(t => t.Id == id);
        }

        //Add a team
        public void AddTeam(Team team)
        {
            _context.Teams.Add(team);
            _context.SaveChanges();
        }

        //check if team with this id exists
        public bool TeamWithIdExists(int id)
        {
           return GetTeams().Exists(t => t.Id == id);
        }

        //remove a team
        public void RemoveTeam(int id)
        {
            _context.Teams.Remove(GetTeamById(id));
            _context.SaveChanges();
        }

        //Update a team
        public void UpdateTeamWithId(int id, Team newTeam)
        {
            Team team = GetTeamById(id);
            team.TeamName = newTeam.TeamName;
            team.HomeVenueId = newTeam.HomeVenueId;
            _context.Teams.Update(team);
            _context.SaveChanges();
        }

        //Patch a team
        public void PatchTeamWithId(int id, [FromBody] JsonPatchDocument<Team> patchDocument)
        {
            Team team = GetTeamById(id);
            patchDocument.ApplyTo(team);
            _context.SaveChanges();
        }
    }
}
