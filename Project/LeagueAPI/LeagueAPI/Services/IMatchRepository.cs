﻿using LeagueAPI.Models;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace LeagueAPI.Services
{
    public interface IMatchRepository
    {
        public List<Match> GetMatches();
        public Match GetMatchById(int id);
        public void AddMatch(Match match);
        public void RemoveMatchById(int id);
        public void UpdateMatchWithId(int id, [FromBody] Match match);
        public void PatchMatchWithId(int id, [FromBody] JsonPatchDocument<Match> patchDocument);
        public bool MatchWithIdExists(int id);
    }
}
