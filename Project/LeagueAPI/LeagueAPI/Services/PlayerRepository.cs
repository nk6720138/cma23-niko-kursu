﻿using LeagueAPI.Models;
using LeagueAPI.Repositories;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace LeagueAPI.Services
{
    public class PlayerRepository : IPlayerRepository
    {
        private readonly LeagueContext _context;
        //Database context
        public PlayerRepository(LeagueContext context)
        {
            _context = context;
        }

        //Get all players
        public List<Player> GetPlayers()
        {
            return _context.Players.ToList();
        }

        //Get a player with the given id
        public Player GetPlayerById(int id)
        {
            Player player = _context.Players.First(p => p.Id == id);
            return player;
        }

        //Add a player
        public void AddPlayer(Player player)
        {
            _context.Players.Add(player);
            _context.SaveChanges();
        }

        //Remove a player
        public void RemovePlayer(Player player)
        {
            _context.Players.Remove(player);
            _context.SaveChanges();
        }

        //check if player with the given id exists
        public bool PlayerWithIdExists(int id)
        {
            return GetPlayers().Exists(p => p.Id == id);
        }

        //Update a player
        public void UpdatePlayerWithId(int id, Player newPlayer)
        {
            Player player = GetPlayerById(id);
            player.Name = newPlayer.Name;
            player.TeamId = newPlayer.TeamId;
            _context.Players.Update(player);
            _context.SaveChanges();
        }

        //Patch a player
        public void PatchPlayerWithId(int id, [FromBody] JsonPatchDocument<Player> patchDocument)
        {
            Player player = GetPlayerById(id);
            patchDocument.ApplyTo(player);
            _context.SaveChanges();
        }
    }
}
