﻿using LeagueAPI.Models;
using LeagueAPI.Repositories;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace LeagueAPI.Services
{
    public class VenueRepository : IVenueRepository
    {
        private readonly LeagueContext _context;
        public VenueRepository(LeagueContext context)
        {
            _context = context;
        }

        public void AddVenue(Venue venue)
        {
            _context.Venues.Add(venue);
            _context.SaveChanges();
        }

        public Venue GetVenueById(int id)
        {
            return _context.Venues.First(v => v.Id == id);
        }

        public List<Venue> GetVenues()
        {
            return _context.Venues.ToList();
        }

        public void PatchVenueWithId(int id, [FromBody] JsonPatchDocument<Venue> patchDocument)
        {
            Venue venue = GetVenueById(id);
            patchDocument.ApplyTo(venue);
            _context.SaveChanges();
        }

        public void RemoveVenue(int id)
        {
            _context.Venues.Remove(GetVenueById(id));
            _context.SaveChanges();
        }

        public void UpdateVenueWithId(int id, [FromBody] Venue newVenue)
        {
            Venue venue = GetVenueById(id);
            venue.Name = newVenue.Name;
            _context.Venues.Update(venue);
            _context.SaveChanges();
        }

        public bool VenueWithIdExists(int id)
        {
            return (GetVenues().Exists(v => v.Id == id));
        }
    }
}
