﻿using LeagueAPI.Models;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace LeagueAPI.Services
{
    public interface IPlayerRepository
    {
        //return all players
        public List<Player> GetPlayers();
        //return a single player with the given id
        public Player GetPlayerById(int id);
        //add a player
        public void AddPlayer(Player player);
        public void RemovePlayer(Player player);
        public void UpdatePlayerWithId(int id, Player player);
        public void PatchPlayerWithId(int id, [FromBody] JsonPatchDocument<Player> patchDocument);
        public bool PlayerWithIdExists(int id);
    }
}
