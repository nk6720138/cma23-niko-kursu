﻿using LeagueAPI.Models;
using LeagueAPI.Repositories;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace LeagueAPI.Services
{
    public class GameRepository : IGameRepository
    {
        private readonly LeagueContext _context;
        public GameRepository(LeagueContext context)
        {
            _context = context;
        }
        public void AddGame(Game game)
        {
            _context.Games.Add(game);
            _context.SaveChanges();
        }

        public bool GameWithIdExists(int id)
        {
            return _context.Games.ToList().Exists(g => g.Id == id);
        }

        public Game GetGameById(int id)
        {
            return _context.Games.First(g => g.Id == id);
        }

        public List<Game> GetGames()
        {
            return _context.Games.ToList();
        }

        public void PatchGameWithId(int id, [FromBody] JsonPatchDocument<Game> patchDocument)
        {
            Game game = GetGameById(id);
            patchDocument.ApplyTo(game);
            _context.SaveChanges();
        }

        public void RemoveGameById(int id)
        {
            _context.Games.Remove(GetGameById(id));
            _context.SaveChanges();
        }

        public void UpdateGameWithId(int id, [FromBody] Game newGame)
        {
            Game game = GetGameById(id);
            game.HomePlayerId = newGame.HomePlayerId;
            game.VisitorPlayerId = newGame.VisitorPlayerId;
            game.HomePlayerScore = newGame.HomePlayerScore;
            game.VisitorPlayerScore = newGame.VisitorPlayerScore;
            game.MatchId = newGame.MatchId;
            _context.Games.Update(game);
            _context.SaveChanges();
        }
    }
}
