﻿using LeagueAPI.Models;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace LeagueAPI.Services
{
    public interface IGameRepository
    {
        public List<Game> GetGames();
        public Game GetGameById(int id);
        public void AddGame(Game game);
        public void RemoveGameById(int id);
        public void UpdateGameWithId(int id, [FromBody]Game game);
        public void PatchGameWithId(int id, [FromBody] JsonPatchDocument<Game> patchDocument);
        public bool GameWithIdExists(int id);
    }
}
