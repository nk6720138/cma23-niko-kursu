﻿using LeagueAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace LeagueAPI.Repositories
{
    public class LeagueContext : DbContext
    {
        public DbSet<Player> Players { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Venue> Venues { get; set; }
        public DbSet<Match> Matches { get; set; }
        public DbSet<Game> Games { get; set; }
        //public DbSet<UserModel> UserModels { get; set; }

        public LeagueContext(DbContextOptions<LeagueContext> options) : base(options) { }

        //slide 36 from asp.net 7. Databases with EF
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //creates table with some starting values
            modelBuilder.Entity<Player>().HasData(
                new Player(12, "NikoK", 123),
                new Player(23, "SamuM", 123),
                new Player(34, "KariV", 123)
                );

            //creates empty table
            //modelBuilder.Entity<Team>().ToTable("Team");

            modelBuilder.Entity<Team>().HasData(
                new Team(1, "Revontuli", 1)
                );

            modelBuilder.Entity<Venue>().HasData(
                new Venue(1, "Revontuli Pub")
                );
        }

    }
}
