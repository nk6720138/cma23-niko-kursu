﻿using System.ComponentModel.DataAnnotations;

namespace LeagueAPI.Models
{
    public class Player
    {
        [Required]
        [Range(1, int.MaxValue)]
        public int Id { get; set; }
        
        [Required]
        [MaxLength(24)]
        public string Name { get; set; }
        
        [Required]
        [Range(1, int.MaxValue)]
        public int TeamId { get; set; }


        /*
        public int GamesWon { get; set; }
        */
        
        public Player()
        {            
        }
        public Player(int id, string name, int teamId)
        {
            Id = id;
            Name = name;
            TeamId = teamId;
            //GamesWon = 0;
        }
    }
}
