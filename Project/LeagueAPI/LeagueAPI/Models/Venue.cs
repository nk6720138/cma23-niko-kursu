﻿using System.ComponentModel.DataAnnotations;

namespace LeagueAPI.Models
{
    public class Venue
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public Venue()
        {
            
        }
        public Venue(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}
