﻿using System.ComponentModel.DataAnnotations;

namespace LeagueAPI.Models
{
    public class Game
    {
        [Required]
        [Range(1, int.MaxValue)]
        public int Id { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int HomePlayerId { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int VisitorPlayerId { get; set; }

        [Required]
        [Range(0, 3)]
        public int HomePlayerScore { get; set; }

        [Required]
        [Range(0, 3)]
        public int VisitorPlayerScore { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int MatchId { get; set; }
    }
}
