﻿using System.ComponentModel.DataAnnotations;

namespace LeagueAPI.Models
{
    public class Match
    {
        [Required]
        [Range(1, int.MaxValue)]
        public int Id { get; set; }

        [Required]
        public DateOnly Date { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int HomeTeamId { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int VisitorTeamId { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int VenueId { get; set; }

        [Required]
        [Range(0, 9)]
        public int HomeTeamScore { get; set; }

        [Required]
        [Range(0, 9)]
        public int VisitorTeamScore { get; set; }
    }
}
