﻿using System.ComponentModel.DataAnnotations;
namespace LeagueAPI.Models
{
    public class Team
    {
        [Required]
        [Range(1, int.MaxValue)]
        public int Id { get; set; }

        [Required]
        public string TeamName { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int HomeVenueId { get; set; }

        public Team()
        {            
        }
        public Team(int id, string name, int homeVenue)
        {
            Id = id;
            TeamName = name;
            HomeVenueId = homeVenue;
        }
    }
}
