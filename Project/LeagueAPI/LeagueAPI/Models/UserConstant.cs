﻿namespace LeagueAPI.Models
{
    // We are not taking data from a database so we get data from constant
    public class UserConstants
    {
        public static List<UserModel> Users = new()
            {
                    new UserModel(){ Username="niko",Password="niko123",Role="Admin"},
                    new UserModel(){ Username="taneli", Password="taneli123",Role="User"}
            };
    }
}
