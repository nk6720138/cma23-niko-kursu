﻿using LeagueAPI.Models;
using LeagueAPI.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace LeagueAPI.Controllers
{
    [Route("api/venues")]
    [ApiController]
    public class VenuesController : ControllerBase
    {
        private readonly IVenueRepository _venueRepository;
        public VenuesController(IVenueRepository venueRepository)
        {
            _venueRepository = venueRepository;
        }

        [AllowAnonymous]
        [HttpGet]
        //Get venues
        public IActionResult GetVenues()
        {
            return Ok(_venueRepository.GetVenues());
        }

        //Get venue by ID
        [AllowAnonymous]
        [HttpGet("{id:int}")]
        public IActionResult GetVenue(int id)
        {
            if (_venueRepository.VenueWithIdExists(id))
            {
                return Ok(_venueRepository.GetVenueById(id));
            }
            return BadRequest();
        }

        //Post a venue
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public IActionResult PostVenue([FromBody] Venue venue)
        {
            if (!TryValidateModel(venue))
            {
                return BadRequest(ModelState);
            }
            if (_venueRepository.VenueWithIdExists(venue.Id))
            {
                return BadRequest("ID in use.");
            }
            _venueRepository.AddVenue(venue);
            return Created(Request.Path, venue);
        }

        //Remove a venue
        [Authorize(Roles = "Admin")]
        [HttpDelete("{id:int}")]
        public IActionResult DeleteVenue(int id)
        {
            if (_venueRepository.VenueWithIdExists(id))
            {
                _venueRepository.RemoveVenue(id);
                return Ok();
            }
            return BadRequest("No player with that id.");
        }

        //Update venue
        [Authorize(Roles = "Admin")]
        [HttpPut("{id:int}")]
        public IActionResult UpdateVenue(int id, [FromBody] Venue newVenue)
        {
            if (_venueRepository.VenueWithIdExists(id))
            {
                newVenue.Id = id;
                _venueRepository.UpdateVenueWithId(id, newVenue);
                return Ok();
            }
            return BadRequest("No such venue.");
        }

        //Patch venue
        [Authorize(Roles = "Admin")]
        [HttpPatch("{id:int}")]
        public IActionResult PatchVenue(int id, [FromBody] JsonPatchDocument<Venue> patchDocument)
        {
            if (_venueRepository.VenueWithIdExists(id))
            {
                _venueRepository.PatchVenueWithId(id, patchDocument);
                return Ok();
            }
            return BadRequest("No such venue.");
        }
    }
}
