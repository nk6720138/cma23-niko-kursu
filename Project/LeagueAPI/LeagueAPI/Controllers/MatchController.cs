﻿using LeagueAPI.Models;
using LeagueAPI.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace LeagueAPI.Controllers
{
    [Route("api/matches")]
    [ApiController]
    public class MatchController : ControllerBase
    {
        private readonly IMatchRepository _matchRepository;
        public MatchController(IMatchRepository matchRepository)
        {
            _matchRepository = matchRepository;
        }

        //Get Match
        [AllowAnonymous]
        [HttpGet]
        public IActionResult Getmatches()
        {
            return Ok(_matchRepository.GetMatches());
        }

        //Get match by ID
        [AllowAnonymous]
        [HttpGet("{id:int}")]
        public IActionResult Getmatch(int id)
        {
            if (_matchRepository.MatchWithIdExists(id))
            {
                return Ok(_matchRepository.GetMatchById(id));
            }
            return BadRequest();
        }

        //Post a match
        [Authorize(Roles = "Admin, User")]
        [HttpPost]
        public IActionResult PostMatch([FromBody] Match match)
        {
            if (!TryValidateModel(match))
            {
                return BadRequest(ModelState);
            }
            if (_matchRepository.MatchWithIdExists(match.Id))
            {
                return BadRequest("ID already in use.");
            }
            _matchRepository.AddMatch(match);
            return Created(Request.Path, match);
        }

        //Remove a match
        [Authorize(Roles = "Admin, User")]
        [HttpDelete("{id:int}")]
        public IActionResult DeleteMatch(int id)
        {
            if (_matchRepository.MatchWithIdExists(id))
            {
                _matchRepository.RemoveMatchById(id);
                return Ok();
            }
            return BadRequest("No such match.");
        }

        //Update match
        [Authorize(Roles = "Admin, User")]
        [HttpPut("{id:int}")]
        public IActionResult UpdateMatch(int id, [FromBody] Match newMatch)
        {
            if (_matchRepository.MatchWithIdExists(id))
            {
                newMatch.Id = id;
                _matchRepository.UpdateMatchWithId(id, newMatch);
                return Ok();
            }
            return BadRequest("No such match.");
        }

        //Patch a match
        [Authorize(Roles = "Admin, User")]
        [HttpPatch("{id:int}")]
        public IActionResult PatchMatch(int id, [FromBody] JsonPatchDocument<Match> patchDocument)
        {
            if (_matchRepository.MatchWithIdExists(id))
            {
                _matchRepository.PatchMatchWithId(id, patchDocument);
                return Ok();
            }
            return BadRequest("No such match");
        }
    }
}
