﻿using LeagueAPI.Models;
using LeagueAPI.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace LeagueAPI.Controllers
{
    [Route("api/games")]
    [ApiController]
    public class GameController : ControllerBase
    {
        private readonly IGameRepository _gameRepository;

        public GameController(IGameRepository gameRepository)
        {
            _gameRepository = gameRepository;
        }

        //get all games
        [AllowAnonymous]
        [HttpGet]
        public IActionResult GetGames()
        {
            return Ok(_gameRepository.GetGames());
        }

        //Get a single game by its ID
        [AllowAnonymous]
        [HttpGet("{id:int}")]
        public IActionResult GetGame(int id)
        {
            if (_gameRepository.GameWithIdExists(id))
            {
                return Ok(_gameRepository.GetGameById(id));
            }
            return NotFound("Game doesn't exist.");
        }

        //Post a game
        [Authorize(Roles = "Admin, User")]
        [HttpPost]
        public IActionResult PostGame([FromBody] Game game)
        {
            if (!TryValidateModel(game))
            {
                return BadRequest(ModelState);
            }
            if (_gameRepository.GameWithIdExists(game.Id))
            {
                return BadRequest("ID in use!");
            }
            _gameRepository.AddGame(game);
            return Created(Request.Path, game);
        }

        //Update Game
        [Authorize(Roles = "Admin, User")]
        [HttpPut("{id:int}")]
        public IActionResult UpdateGame(int id, [FromBody] Game newGame)
        { 
            if (_gameRepository.GameWithIdExists(id))
            {
                newGame.Id = id;
                _gameRepository.UpdateGameWithId(id, newGame);
                return Ok();
            }
            return BadRequest("No such game");
        }

        //Patch a game
        [Authorize(Roles = "Admin, User")]
        [HttpPatch("{id:int}")]
        public IActionResult PatchGame(int id, [FromBody] JsonPatchDocument<Game> patchDocument)
        {
            if (_gameRepository.GameWithIdExists(id))
            {
                _gameRepository.PatchGameWithId(id, patchDocument);
                return Ok();
            }
            return BadRequest("No such game.");
        }

        //Remove a game
        [Authorize(Roles = "Admin, User")]
        [HttpDelete("{id:int}")]
        public IActionResult DeleteGame(int id)
        {
            if (_gameRepository.GameWithIdExists(id))
            {
                _gameRepository.RemoveGameById(id);
                return Ok();
            }
            return BadRequest("No such game.");
        }
    }
}
