﻿using LeagueAPI.Models;
using LeagueAPI.Repositories;
using LeagueAPI.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using System.Numerics;

namespace LeagueAPI.Controllers
{
    [Route("api/players")]
    [ApiController]
    public class PlayerController : ControllerBase
    {
        private readonly IPlayerRepository _playerRepository;

        public PlayerController(IPlayerRepository playerRepository)
        {
            _playerRepository = playerRepository;
        }

        //get all players
        [AllowAnonymous]
        [HttpGet]
        public IActionResult GetPlayers()
        {
            return Ok(_playerRepository.GetPlayers());
        }

        //get a single player by ID
        [AllowAnonymous]
        [HttpGet("{id:int}")]
        //public IActionResult GetPlayer(int id, bool getName...)
        public IActionResult GetPlayer(int id)
        {
            if (_playerRepository.PlayerWithIdExists(id))
            {
                return Ok(_playerRepository.GetPlayerById(id));
            }
            return NotFound("No such player.");
        }

        //Post a new player
        [Authorize(Roles = "Admin, User")]
        [HttpPost]
        public IActionResult PostPlayer([FromBody] Player player)
        {
            if (!TryValidateModel(player))
            {
                return BadRequest(ModelState);
            }
            if (_playerRepository.PlayerWithIdExists(player.Id))
            {
                return BadRequest("ID already in use!");
            }
            _playerRepository.AddPlayer(player);
            return Created(Request.Path, player);
        }

        //Update player
        [Authorize(Roles = "Admin, User")]
        [HttpPut("{id:int}")]
        public IActionResult UpdatePlayer(int id, [FromBody] Player newPlayer)
        {
            if (_playerRepository.PlayerWithIdExists(id))
            {
                newPlayer.Id = id;
                _playerRepository.UpdatePlayerWithId(id, newPlayer);
                return Ok();
            }
            return BadRequest("Player with that ID does not exist.");
        }

        //Patch a player
        [Authorize(Roles = "Admin, User")]
        [HttpPatch("{id:int}")]
        public IActionResult PatchPlayer(int id, [FromBody] JsonPatchDocument<Player> patchDocument)
        {
            if (_playerRepository.PlayerWithIdExists(id))
            {
                _playerRepository.PatchPlayerWithId(id, patchDocument);
                return Ok();
            }
            return BadRequest("Player with that ID does not exist.");
        }

        //Remove a player
        [Authorize(Roles = "Admin, User")]
        [HttpDelete("{id:int}")]
        public IActionResult DeletePlayer(int id)
        {
            if (_playerRepository.PlayerWithIdExists(id))
            {
                Player player = _playerRepository.GetPlayerById(id);
                _playerRepository.RemovePlayer(player);
                return Ok();
            }
            return BadRequest("No player with that id exists.");
        }
    }
}
