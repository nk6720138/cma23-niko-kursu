﻿using LeagueAPI.Models;
using LeagueAPI.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LeagueAPI.Controllers
{
    [Route("api/teams")]
    [ApiController]
    public class TeamController : ControllerBase
    {
        private readonly ITeamRepository _teamRepository;

        public TeamController(ITeamRepository teamRepository)
        {
            _teamRepository = teamRepository;
        }
                
        //get the league leaderboard for teams
        [AllowAnonymous]
        [HttpGet("leaderboard")]
        public IActionResult GetLeaderboard()
        {
            return Ok(_teamRepository.GetLeaderBoard());
        }


        //get all teams
        [AllowAnonymous]
        [HttpGet]
        public IActionResult GetTeams()
        {
            return Ok(_teamRepository.GetTeams());
        }

        //Get a team by its ID
        [AllowAnonymous]
        [HttpGet("{id:int}")]
        public IActionResult GetTeam(int id)
        {
            if (_teamRepository.TeamWithIdExists(id))
            {
                return Ok(_teamRepository.GetTeamById(id));
            }
            return BadRequest("No such team.");
        }

        //Add a team
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public IActionResult PostTeam(Team team)
        {
            if (!TryValidateModel(team))
            {
                return BadRequest(ModelState);
            }
            if (_teamRepository.TeamWithIdExists(team.Id))
            {
                return BadRequest("Team with that ID already exists.");
            }
            _teamRepository.AddTeam(team);
            return Created(Request.Path, team);
        }

        //Delete a team
        [Authorize(Roles = "Admin")]
        [HttpDelete]
        public IActionResult DeleteTeam(int id)
        {
            if (_teamRepository.TeamWithIdExists(id))
            {
                _teamRepository.RemoveTeam(id);
                return Ok();
            }
            return BadRequest("No such team.");
        }

        //Update a team
        [Authorize(Roles = "Admin")]
        [HttpPut("{id:int}")]
        public IActionResult UpdateTeam(int id, [FromBody] Team newTeam)
        {
            if (_teamRepository.TeamWithIdExists(id))
            {
                newTeam.Id = id;
                _teamRepository.UpdateTeamWithId(id, newTeam);
                return Ok();
            }
            return BadRequest("No such team.");
        }

        //Patch a team
        [Authorize(Roles = "Admin")]
        [HttpPatch("{id:int}")]
        public IActionResult PatchTeam(int id, [FromBody] JsonPatchDocument<Team> patchDocument)
        {
            if (_teamRepository.TeamWithIdExists(id))
            {
                _teamRepository.PatchTeamWithId(id, patchDocument);
                return Ok();
            }
            return BadRequest("No such team.");
        }
    }
}
