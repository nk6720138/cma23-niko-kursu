﻿namespace Assignment3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string name = "Niko";
            int age = 221;
            Console.WriteLine(name);
            Console.WriteLine(age); 
            Console.WriteLine("{0} is {1} years old.", name, age);
        }
    }
}