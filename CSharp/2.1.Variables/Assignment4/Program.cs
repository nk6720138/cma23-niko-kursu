﻿namespace Assignment4
{
    internal class Program
    {
        static void Main(string[] args)
        {
            decimal price = 13.40m, increase = 2.1m, result;
            result = price + increase;
            Console.WriteLine("Original price: " + price);
            Console.WriteLine("Increase in price: " + increase);
            Console.WriteLine("Final price: " + result);
            Console.WriteLine("{0:0.00} + {1:0.00} = {2:0.00}", price, increase, result);
        }
    }
}