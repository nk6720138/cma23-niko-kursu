﻿namespace Assignment1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string myName = "Niko";
            int myAge = 5;
            string favouriteLanguage = "C#";
            Console.WriteLine($"name: {myName}, age: {myAge}, favourite language: {favouriteLanguage}");
            myName = "Paavo";
            myAge = 10;
            favouriteLanguage = "C++";
            Console.WriteLine($"name: {myName}, age: {myAge}, favourite language: {favouriteLanguage}");
        }
    }
}