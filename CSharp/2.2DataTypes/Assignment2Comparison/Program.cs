﻿namespace Assignment2Comparison
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int person1Age = 15;
            int person2Age = 24;

            bool isFirstPersonOlder = person1Age > person2Age;
            Console.WriteLine(isFirstPersonOlder);
            //a) Prints false
            //b) It's a boolean

            //c)
            double[] classAGrades = { 9, 6, 9 };
            double[] classBGrades = { 7, 10, 5 };

            double classAAverage = 0, classBAverage = 0;
            for (int i = 0; i < 3; i++)
            {
                classAAverage += classAGrades[i] / 3;
                classBAverage += classBGrades[i] / 3;
            }
            Console.WriteLine(classAAverage > classBAverage);
        }
    }
}