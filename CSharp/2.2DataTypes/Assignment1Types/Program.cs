﻿namespace Assignment1Types
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int appleCount = 13;
            int bananaCount = 5;

            Console.WriteLine("Apples: " + appleCount);
            Console.WriteLine("Bananas: " + bananaCount);
            Console.WriteLine("Fruits in total: " + (appleCount + bananaCount));
        }
    }
}