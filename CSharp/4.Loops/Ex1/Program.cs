﻿namespace Ex1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Your orders, master?");
                string command = Console.ReadLine();
                if (String.Compare(command, "quit") == 0)
                {
                    break;
                }
                if (command.Equals("help"))
                {
                    Console.WriteLine("Type \"help\" for help, \"quit\" to quit");
                }
            }
        }
    }
}