﻿namespace Assignment3Sum
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int sum = 0;
            for (int i = 1; ; i++)
            {
                sum += i;
                if (sum > 10000)
                {
                    Console.WriteLine(i);
                    break;
                }
            }
        }
    }
}