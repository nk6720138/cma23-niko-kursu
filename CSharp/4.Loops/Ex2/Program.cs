﻿namespace Ex2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            PrintPrimesSmallerThan(40);
        }

        //finds primes
        static void PrintPrimesSmallerThan(int n)
        {
            //sieve of eratosthenes
            //all integers to n
            bool[] isPrime = new bool[n + 1];
            for (int i = 0; i <= n; i++)
            {
                isPrime[i] = true;
            }

            for (int i = 2; i * i <= n; i++)
            {
                //if not changed yet, then i is prime
                if (isPrime[i] == true)
                {
                    //set all multiples of i to false
                    for (int j = i * i; j <= n; j += i)
                    {
                        isPrime[j] = false;
                    }
                }
            }
            for (int i = 2; i <= n; i++)
            {
                if (isPrime[i] == true)
                {
                    Console.Write(i + " ");
                }
            }
            Console.WriteLine();
        }
    }
}