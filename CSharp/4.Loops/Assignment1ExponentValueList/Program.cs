﻿namespace Assignment1ExponentValueList
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int n;
            int b;
            Console.WriteLine("Give n: ");
            n = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Give b: ");
            b = Convert.ToInt32(Console.ReadLine());
            if (n <= 0 || b <= 0)
            {
                Console.WriteLine("n and b need to be positive");
            }
            else
            {
                for (int i = 1; i <= n; i++)
                {
                    Console.WriteLine(Math.Pow(b, i));
                }
            }            
        }
    }
}