﻿namespace Assignment4CommandList
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int x = 0, y = 0;
            string commandList = "NNEESSWWCNNEEENNNCEESSSWNNNECEESWWNNNEEEBENNNEEE";

            foreach(char c in commandList)
            {
                if (c == 'B')
                {
                    break;
                }
                if (c == 'C')
                {
                    continue;
                }
                switch (c)
                {
                    case 'N':
                        {
                            y++;
                            break;
                        }
                    case 'S':
                        {
                            y--;
                            break;
                        }
                    case 'E':
                        {
                            x++;
                            break;
                        }
                    case 'W':
                        {
                            x--;
                            break;
                        }
                        default: 
                        {
                            break;
                        }    
                }
            }
            Console.WriteLine("X: " + x + ", Y: " + y);
        }
    }
}