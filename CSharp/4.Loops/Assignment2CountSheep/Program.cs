﻿namespace Assignment2CountSheep
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int n;
            Random rnd = new Random();
            n = rnd.Next(1, 51);
            for (int i = 1; i <= n; i++)
            {
                Console.Write(i + " sheep...");
                if (i % 5 == 0) //linechange to keep it clean
                {
                    Console.WriteLine();
                }
            }
        }
    }
}