﻿using System.ComponentModel.DataAnnotations;

namespace Assignment5AreaOfSquare
{
    internal class Program
    {
        static void Main(string[] args)
        {
            double length = 5.5, area;
            area = Math.Pow(length, 2);
            Console.WriteLine(area);
        }
    }
}