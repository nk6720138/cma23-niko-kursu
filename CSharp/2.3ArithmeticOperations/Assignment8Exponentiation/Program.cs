﻿namespace Assignment8Exponentiation
{
    internal class Program
    {
        static void Main(string[] args)
        {
            double incomeA = 12, incomeB = 8;

            Console.WriteLine(incomeA - incomeB);

            incomeA = Math.Pow(incomeA, 0.9);
            incomeB = Math.Pow(incomeB, 0.9);

            Console.WriteLine(incomeA - incomeB);
        }
    }
}