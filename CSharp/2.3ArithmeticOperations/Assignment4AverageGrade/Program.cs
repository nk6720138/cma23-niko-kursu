﻿namespace Assignment4AverageGrade
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int grade1 = 8;
            int grade2 = 10;
            int grade3 = 7;

            int gradeCount = 3;

            double averageGrade = (double)(grade1 + grade2 + grade3) / gradeCount;

            Console.WriteLine(averageGrade);
        }
    }
}