﻿namespace Assignment6AreaOfRectangle
{
    internal class Program
    {
        static void Main(string[] args)
        {
            double width = 5.5, length = 10, areaOfRectangle, areaOfTriangle;
            areaOfRectangle = width * length;
            areaOfTriangle = 0.5 * width * length;
            Console.WriteLine("The area of the rectangle is {0} and the area of the triangle is {1}", areaOfRectangle, areaOfTriangle);
        }
    }
}