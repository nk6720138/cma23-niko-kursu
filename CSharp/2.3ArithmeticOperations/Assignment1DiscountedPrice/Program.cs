﻿namespace Assignment1DiscountedPrice
{
    internal class Program
    {
        static void Main(string[] args)
        {
            double price = 21.50, discount = 0.3, discountedPrice;
            discountedPrice = (1 - discount) * price;
            Console.WriteLine("Discounted price: {0:0.00}", discountedPrice);
        }
    }
}