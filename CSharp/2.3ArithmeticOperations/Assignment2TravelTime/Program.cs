﻿namespace Assignment2TravelTime
{
    internal class Program
    {
        static void Main(string[] args)
        {
            double distance = 65, speed = 90.0, travelTime, travelTimeInMinutes;
            int travelHours = 0, travelMinutes = 0; // Whole hours and minutes left over

            //distance = speed * time => time = distance/speed
            travelTime = distance / speed;

            //travelTime in minutes = 60*travelTime
            travelTimeInMinutes = travelTime * 60;
            //whole hours
            travelHours = (int)(travelTimeInMinutes / 60);
            //minutes left over
            travelMinutes = (int)(travelTimeInMinutes % 60);

            Console.WriteLine("Travel took {0} hours and {1} minutes.", travelHours, travelMinutes);
        }
    }
}