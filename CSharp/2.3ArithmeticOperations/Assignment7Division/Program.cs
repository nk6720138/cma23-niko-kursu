﻿namespace Assignment7Division
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int pcsOfCandy = 100, peopleSharing = 6;
            double candiesPerPerson = (double)pcsOfCandy / peopleSharing;
            Console.WriteLine("Candies when divided by knife: ");
            Console.WriteLine(candiesPerPerson);
            //extra
            int myExtraCandy = pcsOfCandy%peopleSharing;
            candiesPerPerson = (pcsOfCandy - myExtraCandy)/peopleSharing;
            Console.WriteLine("I got {0} extra candies on top of the {1:0} candies everyone got!", myExtraCandy, candiesPerPerson);
        }
    }
}