﻿namespace Ex1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<string> list1 = GetPopulatedList<string>("Hello, there", 10);
            foreach (string value in list1)
            {
                Console.WriteLine(value);
            }
            List<int> list2 = GetPopulatedList<int>(3, 5);
            foreach (int value in list2)
            {
                Console.WriteLine(value);
            }
        }
        static List<T> GetPopulatedList<T>(T value, int length)
        {
            List<T> list = new List<T>();
            for (int i = 0; i < length; i++)
            {
                list.Add(value);
            }
            return list;
        }
    }
}