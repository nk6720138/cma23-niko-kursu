﻿using System;

namespace Ex2 //includes also ex3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string[] names = File.ReadAllLines(@"C:\Code\Buutti\Homework\CSharp\13.GenericsIEnumerableAndLINQ\Ex2\names.txt");
            string query = "Initial";

            while (query != "quit this")
            {
                Console.WriteLine("Give a string to search for, enter \'quit this\' to quit");
                query = Console.ReadLine();
                //find strings that contain our query as a substring
                string[] queryResults = names
                    .Where(s => s.Contains(query))
                    .ToArray();

                Console.WriteLine(queryResults.Length);
                
                if (queryResults.Length < 10)
                {
                    //users we found with our query
                    List<User> foundUsers = new();

                    for (int i = 0; i < queryResults.Length; i++)
                    {
                        Console.WriteLine(queryResults[i]);                        
                    }
                    //add found names with id's to the list of Users
                    for (int i = 0; i < queryResults.Length; i++)
                    {
                        foundUsers.Add(new User(i + 1, queryResults[i]));
                    }
                    //order by length of name
                    foundUsers = foundUsers.OrderBy(s => s.Name.Length).ToList();
                    for (int i = 0; i < foundUsers.Count; i++)
                    {
                        Console.WriteLine("ID: " + foundUsers[i].Id + ", Name: " + foundUsers[i].Name); 
                    }
                    Console.WriteLine();
                }
            }
        }
    }
    class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public User() { }
        public User(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}