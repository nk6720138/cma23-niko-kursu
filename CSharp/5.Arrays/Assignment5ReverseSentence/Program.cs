﻿namespace Assignment5ReverseSentence
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string sentence = "this is a short sentence";
            string reversed = "";
            // Write your logic here
            for (int i = sentence.Length - 1; i >= 0; i--) 
            {
                reversed += sentence[i];
            }
            Console.WriteLine(reversed); // In this case would print "ecnetnes trohs a si siht"
        }
    }
}