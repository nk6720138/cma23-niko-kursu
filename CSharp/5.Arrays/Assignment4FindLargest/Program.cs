﻿namespace Assignment4FindLargest
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int[] numbers = new int[] { 4, 19, 7, 1, 9, 22, 6, 13 };
            int largest = numbers[0];
            foreach (int number in numbers)
            {
                if (number > largest) largest = number;
            }
            // Write your logic here
            Console.WriteLine(largest); // prints 22
        }
    }
}