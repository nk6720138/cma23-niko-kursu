﻿namespace Assignment3HowManyDays
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int month;
            int[] daysInMonth = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

            Console.WriteLine("What month?: ");
            month = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine(daysInMonth[month - 1]);
        }
    }
}