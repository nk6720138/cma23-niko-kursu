﻿namespace Assignment2Deletion
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<string> fruits = new List<string>() { "banana", "apple", "grapefruit", "pear", "pineapple", "lemon" };
            for (int i = 0; i < fruits.Count; i++)
            {
                if (fruits[i] == "pear")
                {
                    fruits.RemoveAt(i);
                }
            }
        }
    }
}