﻿using System.Security.Cryptography;

namespace Assignment1Palindrome
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string s1, s2, s3, s4;
            s1 = "saippuakivikauppias";
            s2 = "diipadaapa";
            s3 = "okok";
            s4 = "oliananassananailo";

            Console.WriteLine(IsPalindrome(s1));
            Console.WriteLine(IsPalindrome(s2));
            Console.WriteLine(IsPalindrome(s3));
            Console.WriteLine(IsPalindrome(s4));
        }

        static bool IsPalindrome(string s)
        {
            for (int i = 0; i < s.Length/2; i++)
            {
                if (s[i] != s[s.Length - 1 - i])
                {
                    return false;
                }
            }
            return true;
        }
    }
}