﻿namespace Ex1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<string> savedNotes = new List<string>();
            while (true)
            {
                Console.WriteLine("Your orders, master?");
                string command = Console.ReadLine();
                if (String.Compare(command, "quit") == 0)
                {
                    break;
                }
                if (command.Equals("help"))
                {
                    Console.WriteLine("Type \"help\" for help, \"quit\" to quit, \"add\" to add a note," +
                        " \"list\" to print all notes, \"remove\" to remove a note at a given index");
                }
                if (command.Equals("add"))
                {
                    Console.WriteLine("Write a note: ");
                    string note = Console.ReadLine();
                    savedNotes.Add(note);
                }
                if (command.Equals("list"))
                {
                    foreach (string s in savedNotes)
                    {
                        Console.WriteLine($"{s}");
                    }
                }
                if (command.Equals("remove"))
                {
                    for (int i = 0; i < savedNotes.Count; i++)
                    {
                        Console.WriteLine(i + ": " + savedNotes[i]);
                    }
                    int toDelete = int.Parse(Console.ReadLine());
                    savedNotes.RemoveAt(toDelete);
                }
            }
        }
    }
}