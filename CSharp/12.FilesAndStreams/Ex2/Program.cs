﻿using Newtonsoft.Json;
using System;

namespace Ex2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string noteText;
            int noteId = 1;
            List<Note> allNotes = new();

            for (; ; )
            {
                Console.WriteLine("Enter new note, \'q\" to quit: ");
                noteText = Console.ReadLine();
                if (noteText == "q") Environment.Exit(0);
                
                Note newNote = new Note(noteId, DateTime.Now, noteText);
                allNotes.Add(newNote);

                noteId++;

                string jsonString = JsonConvert.SerializeObject(allNotes);
                File.WriteAllText(@"C:\Code\Buutti\Homework\CSharp\12.FilesAndStreams\Ex2\notes.json", jsonString);
            }
        }
    }
    public class Note
    {
        public int Id { get; set; }
        public DateTime TimeStamp { get; set; }
        public string Text { get; set; }
        public Note(){}
        public Note(int id, DateTime time, string text)
        {
            Id = id;
            TimeStamp = time;
            Text = text;
        }
    }
}