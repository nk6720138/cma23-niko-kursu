﻿namespace Ex1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<string> strings = new();

            for (; ; )
            {
                Console.WriteLine("Give a line, quit to quit: ");
                string line = Console.ReadLine();

                switch (line)
                {
                    case "":
                        {
                            //here we ask for a path to a .txt file, and if it exists we replace the content.
                            //if it doesnt exist, we create it
                            saveListToTxt(strings);
                            break;
                        }
                    case "quit":
                        {
                            System.Environment.Exit(0);
                            break;
                        }
                    default:
                        {
                            //here we add the given strings to our list
                            strings.Add(line);
                            break;
                        }
                }
            }
        }
        static void saveListToTxt(List<string> listOfStrings)
        {
            Console.WriteLine("What is the path to your .txt file?");
            string path = Console.ReadLine();
            if (File.Exists(path))
            {
                File.Delete(path);
            }

            File.WriteAllLines(path, listOfStrings);
        }
    }
}