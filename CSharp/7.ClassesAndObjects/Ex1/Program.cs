﻿namespace Ex1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<User> users = new();
            int i = 0;
            while (i < 10)
            {
                User newUser = new();
                Console.WriteLine("Enter username: ");
                newUser.userName = Console.ReadLine();
                Console.WriteLine("Enter password: ");
                newUser.passWord = Console.ReadLine();
                users.Add(newUser);
                Console.WriteLine($"Usernames:");
                foreach (User user in users)
                {
                    Console.WriteLine(user.userName);

                }
                i++;
            }
        }
    }
    class User
    {
        public string? userName;
        public string? passWord;
    }
}