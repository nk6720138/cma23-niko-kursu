﻿using TestNameSpace; //ex3
namespace Ex2
{
    internal class Program
    {
        static void Main(string[] args)
        {            
            Animal dog = new()
            {
                Name = "Luci",
                Sound = "wouuu"
            };
            Animal cat = new()
            {
                Name = "Kisu",
                Sound = "mauu"
            };
            dog.Greet();
            cat.Greet();
            
            //ex3
            TestClass testClassInstance = new TestClass();

            //ex4
            Animal dog2 = new("Hurtta", "Räyh");
            Animal cat2 = new("Katti", "hshshshshssss");
            dog2.Greet();
            cat2.Greet();
        }
    }
    class Animal
    {
        string name;
        string sound;
        public string Name { get; set; }
        public string Sound { get; set; }

        //ex4 ?
        public Animal()
        {            
        }
        public Animal(string name, string sound)
        {
            Name = name;
            Sound = sound;
        }

        public void Greet()
        {
            Console.WriteLine(Name + " says " + Sound + "!");
        }
    }
}