﻿namespace Ex1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int a, b;
            Random rnd = new Random();
            for (int i = 0; i < 5; i++)
            {
                a = rnd.Next(0, 101);
                b = rnd.Next(0, 101);
                Sum(a, b);
                Difference(a, b); 
            }
        }
        static void Sum(int value1, int value2)
        {
            Console.WriteLine("The sum of " + value1 + " and " + value2 + " is " + (value1 + value2));
        }
        static void Difference(int value1, int value2)
        {
            Console.WriteLine("The difference of " + value1 + " and " + value2 + " is " + (value1 - value2));
        }
    }

}