﻿namespace Assignment1SimpleFunction
{
    public static class Global
    {
        public static string language;
    }
    internal class Program
    {
        //static string language;
        static void Main(string[] args)
        {
            Global.language = "en";
            Hello();
            Global.language = "fr";
            Hello();
            Global.language = "fi";
            Hello();

            string lang = "en";
            Hello2(lang);
            lang = "fr";
            Hello2(lang);
            lang="fi";
            Hello2(lang);
        }
        static void Hello()
        {
            if (Global.language == "en")
            {
                Console.WriteLine("Hello, World!");
            }
            if (Global.language == "fr")
            {
                Console.WriteLine("Bonjour le monde!");
            }
            if (Global.language == "fi")
            {
                Console.WriteLine("Päivää maailma!");
            }
        }
        static void Hello2(string lang)
        {
            if (lang == "en")
            {
                Console.WriteLine("Hello, World!");
            }
            if (lang == "fr")
            {
                Console.WriteLine("Bonjour le monde!");
            }
            if (lang == "fi")
            {
                Console.WriteLine("Päivää maailma!");
            }
        }
    }
}