﻿namespace Ex2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string spacelessString = SpacesInString("Write a method that takes a string as a parameter");
            Console.WriteLine(spacelessString);
        }
        static string SpacesInString(string s)
        {
            string noSpaces = "";
            for (int i = 0; i < s.Length; i++)
            {
                char c = s[i];
                if (c != ' ')
                {
                    noSpaces += c;
                }
            }
            return noSpaces;
        }
    }
}