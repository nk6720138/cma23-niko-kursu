﻿namespace Ex3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            UniqueLetterPrinter("Hello, world");
            LetterCountPrinter("Hello, world");
        }
        static void UniqueLetterPrinter(string s)
        {
            HashSet<char> hSet = new HashSet<char>();
            foreach (char c in s)
            {
                if (Char.IsLetter(c))
                {
                    hSet.Add(c);
                }
            }
            foreach (char c in hSet)
            {
                Console.Write(c);
            }
            Console.WriteLine();
        }
        static void LetterCountPrinter(string s)
        {
            //we consider uppercase and lowercase letters to be the same
            string lowerCaseS = s.ToLower();
            Dictionary<char, int> charDic = new Dictionary<char, int>();
            foreach (char c in lowerCaseS)
            {
                if (Char.IsLetter(c))
                {
                    if (charDic.ContainsKey(c))
                    {
                        charDic[c]++;
                    }
                    else
                    {
                        charDic.Add(c, 1);
                    }
                }
            }
            foreach (KeyValuePair<char, int> kvp in charDic)
            {
                Console.WriteLine("Number of  {0}'s: {1}", kvp.Key, kvp.Value);
            }
        }
    }
}