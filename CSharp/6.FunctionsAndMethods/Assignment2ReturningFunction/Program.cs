﻿namespace Assignment2ReturningFunction
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int x = 22, y = 11, z = 3;
            Console.WriteLine(Minimum(x, y, z));
        }
        static int Minimum(int a, int b, int c)
        {
            int smallest = a;
            if (b < smallest) smallest = b;
            if (c < smallest) smallest = c;

            return smallest;
        }
    }
}