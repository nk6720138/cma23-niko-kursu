﻿namespace Ex4
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int numberToSort;
            string input = "0";

            //asks for a number until one satisfying the conditions is given
            while (input.Length != 10 || !CheckIfInt(input))
            {
                Console.WriteLine("Input a 10-digit integer: ");
                input = Console.ReadLine();
            }
            //parse the string that now is quaranteed to satisfy the conditions to int. 
            numberToSort = int.Parse(input);

            (List<int> evens, List<int> odds) = DigitSorter(numberToSort);            

            foreach (int i in evens)
            {
                Console.Write(i + " ");
            }
            Console.WriteLine();

            foreach (int i in odds)
            {
                Console.Write(i + " ");
            }
            Console.WriteLine();
        }
        //Sorts the digits of a given number to odds and evens. This method assumes the given number is always
        //10 digits long, so for example 12345678 will be treated as 0012345678
        static (List<int>, List<int>) DigitSorter(int number)
        {
            int digitsInNumber = CountDigits(number);
            List<int> evens = new();
            List<int> odds = new();
            for (int i = 0; i < digitsInNumber; i++)
            {
                int digit = number % 10;
                if (digit % 2 == 0)
                {
                    evens.Add(digit);
                }
                else
                {
                    odds.Add(digit);
                }
                number /= 10;
            }
            //if numberToSort starts with zeroes, for example 0123456789, we add the zeroes to the evens.
            //technically though, numbers don't start with zeroes, but series of digits might
            for (int i = 0; i < (10 - digitsInNumber); i++)
            {
                evens.Add(0);
            }
            return (evens, odds);
        }
        //counts digits in a number, not really necessary here since the length is always 10
        static int CountDigits(int number)
        {
            int digits = 0;
            while (number > 0)
            {
                digits++;
                number /= 10;
            }
            return digits;
        }
        //checks if given string represents an integer
        static bool CheckIfInt(string s)
        {
            for (int i = 0; i < s.Length; i++)
            {
                if (!Char.IsDigit(s[i])) return false;
            }
            return true;
        }
    }
}