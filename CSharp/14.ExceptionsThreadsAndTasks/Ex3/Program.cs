﻿namespace Ex3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Task loadingTask;
                ConsoleKeyInfo command = Console.ReadKey();
                if (command.Key == ConsoleKey.Enter)
                {
                    loadingTask = LoadDataAsync();
                    //loadingTask.Wait();
                    //Console.WriteLine("Loading done.");
                }
                if (command.Key == ConsoleKey.Escape)
                {
                    Environment.Exit(0);
                }
                if (command.Key != ConsoleKey.Enter && command.Key != ConsoleKey.Escape)
                {
                    Console.WriteLine("\nNot a valid command, enter to load, esc to quit");
                    continue;
                }
                //loadingTask.Wait();
                //Console.WriteLine("Loading done.");
            }
        }
        static async Task LoadDataAsync()
        {
            int waitTime = 10;
            Console.WriteLine("Loading");
            for (int i = 0; i <= 100; i++)
            {
                await Task.Delay(waitTime);
                Console.WriteLine($"Progress: {i}%");
            }
            Console.WriteLine("Loading done!");
        }
    }
}