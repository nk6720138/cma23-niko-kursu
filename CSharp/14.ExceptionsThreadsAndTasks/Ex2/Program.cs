﻿namespace Ex2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                ConsoleKeyInfo command = Console.ReadKey();
                if (command.Key == ConsoleKey.Enter)
                {
                    ThreadStart loadDataStart = new ThreadStart(LoadData);
                    Thread loadDataThread = new Thread(LoadData);
                    loadDataThread.Start(); 
                }
                if (command.Key == ConsoleKey.Escape)
                {
                    Environment.Exit(0);
                }                
            }
        }
        public static void LoadData()
        {
            Console.WriteLine("Loading");
            for (int i = 0; i <= 100; i++)
            {
                Thread.Sleep(10);
                Console.WriteLine($"Progress: {i}%");
            }
        }
    }
}