﻿using System;

namespace Ex1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            for (; ; )
            {
                Console.WriteLine("How much money are you looking to save?");
                uint target, salary;
                try
                {
                    target = uint.Parse(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("Only enter positive integers please!");
                    continue;
                }
                catch (OverflowException)
                {
                    Console.WriteLine("Only enter positive integers please!");
                    continue;
                }

                Console.WriteLine("What's your monthly salary?");
                try
                {
                    salary = uint.Parse(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("Only enter positive integers please!");
                    continue;
                }
                catch (OverflowException)
                {
                    Console.WriteLine("Only enter positive integers please!");
                    continue;
                }

                uint monthsNeeded;

                try
                {
                    if (target % salary == 0)
                    {
                        monthsNeeded = target / salary;
                    }
                    else
                    {
                        monthsNeeded = (target / salary) + 1;
                    }
                }
                catch (DivideByZeroException)
                {
                    Console.WriteLine("You can't work for free!");
                    continue;                    
                }                
                Console.WriteLine($"It will take you {monthsNeeded} months to save up to your target.");

                Console.WriteLine("Where do you want to save this result?");
                string pathToFolder = Console.ReadLine();
                // C:\Code\Buutti\Homework\CSharp\14.ExceptionsThreadsAndTasks\Ex1\FolderForResults\
                try
                {
                    File.WriteAllText(@pathToFolder + "months_until_target.txt", monthsNeeded.ToString());
                }
                catch (DirectoryNotFoundException)
                {
                    Console.WriteLine("Directory does not exists!");
                    continue;
                }
                catch (PathTooLongException)
                {
                    Console.WriteLine("Path too long!");
                    continue;
                }
            }
        }
    }
}