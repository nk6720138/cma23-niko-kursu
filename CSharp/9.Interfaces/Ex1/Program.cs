﻿namespace Ex1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Product milk = new("Straight from a titty!");
            Product salsa = new("Hot hot");
            Category customers = new("People with money");
            Category sellers = new("People with product");

            List<IInfo> infos = new()
            {
                milk, salsa, sellers, customers
            };

            Console.WriteLine("Press enter, or input \"q\" to quit.");
            string input = Console.ReadLine();

            while (input != "q")
            {
                foreach (IInfo info in infos)
                {
                    info.PrintInfo();
                }
                input = Console.ReadLine();
            }
        }
    }

    interface IInfo
    {
        string InfoText { get; set; }
        void PrintInfo() { }
    }
    class Product : IInfo
    {
        public string InfoText { get; set; }
        public Product(string text) => this.InfoText = text;
        public void PrintInfo() => Console.WriteLine(InfoText);
    }
    class Category : IInfo
    {
        public string InfoText { get; set; }
        public Category(string text) => this.InfoText = text;
        public void PrintInfo() => Console.WriteLine(InfoText);
    }
}