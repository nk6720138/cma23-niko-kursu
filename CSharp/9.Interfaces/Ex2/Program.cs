﻿namespace Ex2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Square square1 = new(4.2, 5.0);
            Square square2 = new(5.0, 8.2);
            Square square3 = new(10, 10);
            Triangle triangle1 = new(10.4, 5.88);
            Triangle triangle2 = new(10, 10);
            Circle circle1 = new(5.7);
            Circle circle2 = new(100.0);

            List<Shape> shapes = new()
            {
                square1, square2, square3, triangle1, triangle2, circle1, circle2
            };

            shapes.Sort();

            foreach (Shape shape in shapes)
            {
                Console.WriteLine(shape.Area);
            }
        }
    }
    class Shape : IComparable<Shape>
    {
        public double Area;
        public int CompareTo(Shape? other)
        {
            if (Area > other.Area) return 1;
            else if (Area == other.Area) return 0;
            else return -1;
        }
    }
    class Square : Shape
    {
        public Square(double length, double height)
        {
            Area = length * height;
        }
    }
    class Triangle : Shape
    {
        public Triangle(double length, double height)
        {
            Area = 0.5 * length * height;
        }
    }
    class Circle : Shape
    {
        public Circle(double radius)
        {
            Area = double.Pi * Math.Pow(radius, 2);
        }
    }
}