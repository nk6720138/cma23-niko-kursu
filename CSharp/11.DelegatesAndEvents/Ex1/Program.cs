﻿namespace Ex1
{
    delegate void DoTheProcedure();
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Orders, Master?");
            DoTheProcedure executor = new(delegate { });

            for (; ; )
            {
                Console.WriteLine("Valid commands:");
                Console.WriteLine("ReleaseWater");
                Console.WriteLine("IncreaseTemperature");
                Console.WriteLine("ReleaseFertilizer");
                Console.WriteLine("run");
                Console.WriteLine("quit");
                Console.WriteLine("clear");
                Console.WriteLine("");
                string? command = Console.ReadLine();
                switch (command)
                {
                    case "ReleaseWater":
                        {
                            executor += ReleaseWater;
                            Console.WriteLine("");
                            break;
                        }
                    case "IncreaseTemperature":
                        {
                            executor += IncreaseTemperature;
                            Console.WriteLine("");
                            break;
                        }
                    case "ReleaseFertilizer":
                        {
                            executor += ReleaseFertilizer;
                            Console.WriteLine("");
                            break;
                        }
                    case "run":
                        {
                            executor();
                            Console.WriteLine("");
                            break;
                        }
                    case "quit": //exit app
                        {
                            System.Environment.Exit(0);
                            break;
                        }
                    case "clear": //clear commands
                        {
                            executor = new(delegate { });
                            Console.WriteLine("");
                            break;
                        }
                    default:
                        {
                            Console.WriteLine("Please give a valid command.");
                            Console.WriteLine("");
                            break;
                        }
                }
            }
        }
        static void ReleaseWater()
        {
            Console.WriteLine("Releasing Water...");
        }
        static void ReleaseFertilizer()
        {
            Console.WriteLine("Releasing fertilizer...");
        }
        static void IncreaseTemperature()
        {
            Console.WriteLine("Increasing temperature...");
        }
    }
}