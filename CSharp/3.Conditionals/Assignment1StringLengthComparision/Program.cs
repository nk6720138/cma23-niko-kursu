﻿namespace Assignment1StringLengthComparision
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string s1 = "saUABHPFdsfsdf", s2 = "brbtrbrt";
            if (s1.Length == s2.Length)
            {
                Console.WriteLine(s1 + " is as long as " + s2);
            }
            else if (s1.Length < s2.Length)
            {
                Console.WriteLine(s1 + " is shorter than " + s2);
            }
            else
            {
                Console.WriteLine(s1 + " is longer than " + s2);
            }
        }
    }
}