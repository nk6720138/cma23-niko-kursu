﻿namespace Assignment4LogicalOperators
{
    internal class Program
    {
        static void Main(string[] args)
        {
            double score = 5, hoursPlayed = 20, price = 9;

            if ((score >= 4 && price == 0) || (score == 4 && (hoursPlayed / price >= 4))
                || (score == 5 && (hoursPlayed / price >= 2)))
            {
                Console.WriteLine("Worth!");
            }
            else
            {
                Console.WriteLine("Not worth..");
            }
        }
    }
}