﻿namespace Assignment5FlowChartATM
{
    internal class Program
    {
        static void Main(string[] args)
        {
            double balance = 99.99;
            bool checkBalance = true, isActive = true;

            if (checkBalance)
            {
                if (isActive && balance > 0)
                {
                    Console.WriteLine($"Your balance: {balance}");
                }
                else if(!isActive)
                {
                    Console.WriteLine("Your account is not active");
                }
                else
                {
                    if (balance == 0)
                    {
                        Console.WriteLine("Your account is empty");
                    }
                    else
                    {
                        Console.WriteLine("Your balance is negative");
                    }
                }
            }
            else
            {
                Console.WriteLine("Have a nice day!");
            }
        }
    }
}