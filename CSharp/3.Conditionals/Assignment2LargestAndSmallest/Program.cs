﻿namespace Assignment2LargestAndSmallest
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int number1 = 11, number2 = 2, number3 = 333, smallest, largest;
            smallest = number1;
            largest = number1;

            if (number1 == number2 && number2 == number3 )
            {
                Console.WriteLine("All equal");
            }

            if (number2 < smallest) smallest = number2;
            if (number3 < smallest) smallest= number3;

            if (number2 > largest) largest = number2;
            if (number3 > largest) largest= number3;

            Console.WriteLine("Smallest is {0} and largest is {1}", smallest, largest);
        }
    }
}