﻿namespace Assignment3HowManyDays
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Which month?");
            int givenMonth = Convert.ToInt32(Console.ReadLine());
            switch (givenMonth)
            {
                case 1: case 3: case 5: case 7:
                case 8: case 10: case 12:
                    {
                        Console.WriteLine("31");
                        break;
                    }
                case 2:
                    {
                        Console.WriteLine("28");
                        break;
                    }
                case 4: case 6: case 9: case 11:
                    {
                        Console.WriteLine("30");
                        break;
                    }
                    default:
                    {
                        Console.WriteLine("Not a valid month!");
                        break;
                    }
            }
        }
    }
}