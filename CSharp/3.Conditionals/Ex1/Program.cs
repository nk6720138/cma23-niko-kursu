﻿namespace Ex1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("What weekday is it?");
            string weekDay = Console.ReadLine();
            switch (weekDay)
            {
                case "monday":
                case "tuesday":
                case "wednesday":
                case "thursday":
                    {                        
                        Console.WriteLine("Have a nice week!");
                        break;
                    }
                    default:
                    {
                        Console.WriteLine("Have a nice weekend!");
                        break;
                    }
            }            
        }
    }
}