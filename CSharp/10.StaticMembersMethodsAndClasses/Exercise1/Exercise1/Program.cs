﻿namespace Exercise1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<Message> allMessages = new();

            Console.WriteLine("Enter new message, or \"q\" to quit: ");
            string ? userInput = Console.ReadLine();
            while(userInput != "q")
            {
                Message newMessage = new(userInput);
                allMessages.Add(newMessage);
                Console.WriteLine($"Total messages: {Message.TotalMessages}, last message: {Message.LastMessage}");
                Console.WriteLine("Enter new message, or \"q\" to quit: ");
                userInput = Console.ReadLine();
            }
        }
    }
    class Message
    {
        public static int TotalMessages;
        public static string LastMessage;
        string ? MessageText;
        public Message(string message)
        {
            TotalMessages++;
            LastMessage = message;
        }
    }
}