﻿namespace _4.MoreCodeFixing
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int number = 5;
            int increase = 4;
            int limit = 11;

            number += increase;

            /*
             * Print number if it is 
             * bigger than limit (11)
             * 
            */
            if (number > limit)
            {
                Console.WriteLine(number);
            }

            number += increase;

            // Check if it is bigger after it has been increased again
            if (number > limit)
            {
                Console.WriteLine(number);
            }
        }
    }
}