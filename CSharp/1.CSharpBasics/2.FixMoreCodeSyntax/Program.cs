﻿namespace _2.FixMoreCodeSyntax
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // First print 5 and then print 7
            int number = 5;
            Console.WriteLine(number);
            number = 7;
            Console.WriteLine(number);
        }
    }
}