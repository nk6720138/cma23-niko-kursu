﻿namespace _3.CodeCleanUp
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int value = 2;
            //Console.WriteLine(value);

            int increase = 3;
            //Console.WriteLine(increase);

            value = value + increase;
            //Console.WriteLine(value);
            //Console.WriteLine(increase);

            //int newValue = value + increase;
            //Console.WriteLine(newValue);
            //Console.WriteLine(increase);
            value = value + increase;

            int increaseMinusOne = increase - 1;
            value = value + increaseMinusOne;
            Console.WriteLine("result: " + value);
            //Console.WriteLine(increaseMinusOne);
        }
    }
}