﻿using System.Xml.Linq;
using System;

namespace FixCodeSyntax
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string userName = "Juliet";
            // this is a comment
            // we greet the user
            Console.WriteLine("Hello {0}", userName);
            //Console.WriteLine(userName);
        }
    }
}